package com.l.agendaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final ArrayList<Contacto> contactos = new ArrayList<Contacto>();
    EditText nombre_et;
    EditText telefono1_et;
    EditText telefono2_et;
    EditText direccion_et;
    EditText notas_et;
    CheckBox favorito_cb;
    Contacto saveContact;
    Button borrar_btn;
    int savedIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nombre_et =(EditText)findViewById(R.id.nombre_et);
        telefono1_et =(EditText)findViewById(R.id.telefono1_et);
        telefono2_et =(EditText)findViewById(R.id.telefono2_et);
        direccion_et = (EditText)findViewById(R.id.direccion_et);
        notas_et = (EditText)findViewById(R.id.notas_et);
        favorito_cb = (CheckBox)findViewById(R.id.favorito_cb);
        Button guardar_btn = (Button)findViewById(R.id.guardar_btn);
        Button limpiar_btn = (Button)findViewById(R.id.limpiar_btn);
        borrar_btn = (Button)findViewById(R.id.borrar_btn);
        Button listar_btn = (Button)findViewById(R.id.listar_btn);

        saveContact=null;
        savedIndex=0;
        borrar_btn.setEnabled(false);

        guardar_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(     nombre_et.getText().toString().equals("") ||
                        direccion_et.getText().toString().equals("") ||
                        telefono1_et.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.mensajeerror, Toast.LENGTH_SHORT).show();
                }else{
                    Contacto nContacto = new Contacto();
                    int index= contactos.size();
                    if(saveContact !=null){
                        contactos.remove(savedIndex);
                        nContacto=saveContact;
                        index = savedIndex;
                    }
                    nContacto.setNombre(nombre_et.getText().toString());
                    nContacto.setTelefono1(telefono1_et.getText().toString());
                    nContacto.setTelefono2(telefono2_et.getText().toString());
                    nContacto.setDireccion(direccion_et.getText().toString());
                    nContacto.setNotas(notas_et.getText().toString());
                    nContacto.setFavorito(favorito_cb.isChecked());
                    contactos.add(index, nContacto);
                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    saveContact=null;
                    limpiar();
                }
            }
        });


        if(saveContact !=null){
            borrar_btn.setEnabled(true);
            contactos.remove(savedIndex);
        }
        borrar_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Contacto nContacto = new Contacto();
                    int index= contactos.size();

                    contactos.remove(index-1);
                    Toast.makeText(MainActivity.this, "Contacto Borrado", Toast.LENGTH_SHORT).show();
                    saveContact=null;
                    limpiar();
                    Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                    Bundle objeto = new Bundle();
                    objeto.putSerializable("contactos", contactos);
                    intent.putExtras(objeto);
                    startActivityForResult(intent,0);
            }
        });

        listar_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                Bundle objeto = new Bundle();
                objeto.putSerializable("contactos", contactos);
                intent.putExtras(objeto);
                startActivityForResult(intent,0);
            }
        });

        limpiar_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    public void limpiar(){
        saveContact=null;
        nombre_et.setText("");
        telefono1_et.setText("");
        telefono2_et.setText("");
        direccion_et.setText("");
        notas_et.setText("");
        favorito_cb.setChecked(false);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null){
            Bundle oBundle = intent.getExtras();
            saveContact = (Contacto)oBundle.getSerializable("contacto");
            savedIndex= oBundle.getInt("index");
            nombre_et.setText(saveContact.getNombre());
            telefono1_et.setText(saveContact.getTelefono1());
            telefono2_et.setText(saveContact.getTelefono2());
            direccion_et.setText(saveContact.getDireccion());
            notas_et.setText(saveContact.getNotas());
            favorito_cb.setChecked(saveContact.getFavorito());
            borrar_btn.setEnabled(true);
        }else{
            borrar_btn.setEnabled(false);
            limpiar();
        }
    }
}
