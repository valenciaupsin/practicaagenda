package com.l.agendaapp;

import java.io.Serializable;

class Contacto implements Serializable {
    private int id;
    private String nombre;
    private String telefono1;
    private String telefono2;
    private String direccion;
    private String notas;
    private Boolean favorito;

    public Contacto() {
        id++;
    }

    public Contacto(Contacto contacto) {
        this.id = contacto.id;
        this.nombre = contacto.nombre;
        this.telefono1 = contacto.telefono1;
        this.telefono2 = contacto.telefono2;
        this.direccion = contacto.direccion;
        this.notas = contacto.notas;
        this.favorito = contacto.favorito;
    }

    public Contacto(String nombre, String telefono1, String telefono2, String direccion, String notas, Boolean favorito) {
        id++;
        this.nombre = nombre;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.direccion = direccion;
        this.notas = notas;
        this.favorito = favorito;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public Boolean getFavorito() {
        return favorito;
    }

    public void setFavorito(Boolean favorito) {
        this.favorito = favorito;
    }
}
