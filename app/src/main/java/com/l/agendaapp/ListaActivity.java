package com.l.agendaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

    TableLayout lista_tbl;
    ArrayList<Contacto> contactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        lista_tbl = findViewById(R.id.tblLista);

        Bundle objeto = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) objeto.getSerializable("contactos");

        Button nuevo_btn = findViewById(R.id.nuevo_btn);
        nuevo_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContacto();

    }

    public void cargarContacto(){
        for(int x=0; x < contactos.size(); x++){
            Contacto c = new Contacto(contactos.get(x));
            TableRow nRow = new TableRow(ListaActivity.this);

            TextView nombre = new TextView(ListaActivity.this);
            nombre.setText(c.getNombre());

            nombre.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nombre.setTextColor((c.getFavorito())? Color.BLUE:Color.BLACK);
            nRow.addView(nombre);

            Button ver_btn = new Button(ListaActivity.this);
            ver_btn.setText(R.string.accion_ver);
            ver_btn.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            ver_btn.setTextColor(Color.BLACK);

            ver_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c = (Contacto) v.getTag(R.string.contacto_g);
                    Intent i = new Intent();
                    Bundle objeto = new Bundle();
                    objeto.putSerializable("contacto", c);
                    objeto.putInt("index", Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(objeto);
                    setResult(RESULT_OK,i);
                    finish();
                }
            });
            ver_btn.setTag(R.string.contacto_g,c);
            ver_btn.setTag(R.string.contacto_g_index,x);

            nRow.addView(ver_btn);
            lista_tbl.addView(nRow);


        }
    }
}
